<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>983</title>
</head>
<body>
    <?php
        while (true) {
            $numero = rand(1, PHP_INT_MAX); 

            if ($numero % 983 == 0) {
                echo "El número generado ($numero) es divisible por 983. ¡Se cumplió la condición!";
                break;
            }
        }
    ?>

</body>
</html>