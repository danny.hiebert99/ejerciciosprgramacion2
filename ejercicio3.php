<!DOCTYPE html>
<html>
<head>
    <title>Tipo de Datos</title>
</head>
<body>
    <?php
    $variable = 24.5;
    echo 'Tipo de datos de la variable: ' . gettype($variable) . "<br>";
    $variable = "HOLA";
    echo 'Tipo de datos de la variable: ' . gettype($variable) . "<br>";
    settype($variable, "integer");
    echo 'Contenido y tipo de la variable después del cambio: ';
    var_dump($variable);
    ?>
</body>
</html>
