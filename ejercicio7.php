<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aleatorio</title>
</head>
<body>
    <?php
        $contador = 0;

        while ($contador < 900) {
            $numero = rand(1, 10000);

            if ($numero % 2 == 0) {
                echo $numero . " - ";
                $contador++;
            }
        }
    ?>

</body>
</html>