<!DOCTYPE html>
<html>
<head>
    <title>Tipo de Datos</title>
</head>
<body>
    <?php
    $a = rand(99, 999);
    $b = rand(99, 999);
    $c = rand(99, 999);

    $result = ($a * 3 > $b + $c) ? "The expression {$a}*3 (". ($a*3) .") is greater than {$b}+{$c} (". ($b+$c) .")" : "The expression {$b}+{$c} (". ($b+$c) .") is greater or equal to {$a}*3 (". ($a*3) .")";

    echo $result;
    ?>
</body>
</html>
